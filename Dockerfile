FROM python:3-alpine

ENV user admin
ENV email admin@example.com
ENV password pass
ENV file db/db.sqlite3

WORKDIR /code
COPY . /code

RUN mkdir /code/db
RUN pip3 install -r requirements.txt

EXPOSE 8000

CMD sh init.sh && python manage.py runserver 0.0.0.0:8000